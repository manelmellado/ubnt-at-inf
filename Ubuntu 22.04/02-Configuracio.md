
# Configuració d'Ubuntu 22.04

# Curs 2022-2023

    Notació: a partir d’ara si veiem el caràcter `#` davant d’una ordre que hem
    d’executar voldrà dir que per escriure-la haurem de ser l’usuari _root_ (el _super
    usuari_ o administrador). I aquest caràcter `#` no l’hem de posar.
## Punts clau on posar atenció

* És obligatori seguir sempre l’ordre de les operacions presentades.

## Configuració

1. Iniciem sessió gràfica com l’usuari _guest_. Obrim el _Firefox_ i un terminal;
   disposem les finestres per veure-les simultàniament.

    * En el navegador visualitzem les instruccions en la pàgina de
      **Configuració** dins del repositori de _Git_ mencionat més amunt.
    * En el terminal ens convertim en administrador executant `sudo su -l`.
    * Un cop a root realitzarem l'ordre passwd i modificarem el password de root a jupiter.

    ```
    # passwd
    ```
    * A un altre terminal o pestanya, comprobem que la password ha sigut modificada correctament fent su -l desde l'usuari guest i utilitzant la password jupiter.
    * **A un nou terminal**
    ```
    # passwd
    ```

    * Si tot ha anat bé i podem entrar amb root, retirem els permisos de root a guest amb:
    ```
    # gpasswd -d guest sudo
    ```


2. Instal·lació d'editors i gestors de fitxers i altres paquets.

    En comptes de fer servir l’editor vi farem servir vim (life is colorful!).

    L’instal·lem com a _root_:

    ```
    # apt-get -y install vim vim-gtk3 mc geany aptitude tree git openssh-server && systemctl disable ssh
    ```


3. Configuracions pròpies del departament d'Informàtica.

    Instal·lació de dependències:

    ```
    # apt-get install krb5-user krb5-multidev libpam-mount sssd nfs-common autofs ufw curl -y 
    ```  

    _Acceptem TOTES les preguntes posteriors que ens farà el procés de configuració. O sigui, NO ESCRIVIM RESº_


    Descarreguem els arxius de configuració que trobareu al git, cal copiar-los al directori /etc/sudoers.d/ i /etc/sssd/:

    [Configuració](/Ubuntu 22.04/arxius/etc)

    ```
    # git clone https://gitlab.com/manelmellado/ubnt-at-inf.git
    # cd ubnt-at-inf/Ubuntu\ 22.04/arxius/etc/
    # sudo cp sssd/sssd.conf /etc/sssd/
    # sudo cp sudoers.d/inf /etc/sudoers.d/
    ```

    És necessari modificar els permisos de l'arxiu sssd.conf a 600 o no funcionarà
    ```
    # chmod 600 /etc/sssd/sssd.conf
    ```

    
    Un cop copiats realitzem la següent ordre, recordant marcar ( amb l'espai ) la casella d'autocreació de home en inici
    + Create home directory on login

    ```
    # pam-auth-update
    ```


4. Snap Erros

   El gestor de paquets d'snap dona errors amb el usuaris d'ldap degut a la gestió de homes.
   Serà necessari modificar l'arxiu [site.local](https://gitlab.com/manelmellado/ubnt-at-inf/-/blob/main/Ubuntu%2022.04/arxius/site.local)
   que trobareu a aquest enllaç i sobreescriure el del sistema a /etc/apparmor.d/tunables/home.d/site.local

    ```
    # cd ubnt-at-inf/Ubuntu\ 22.04/arxius/
    # cp site.local /etc/apparmor.d/tunables/home.d/site.local
    ```
   Important assegurar-se que els permissos queden amb 611 com eren originalment
    ```
    # ls -la /etc/apparmor.d/tunables/home.d/site.local
    -rw-r--r-- 1 root root 684 sep  7 18:21 /etc/apparmor.d/tunables/home.d/site.local
    ```

5. Ara cal reiniciar el sistema executant en el terminal l’ordre `reboot`. **Mai** s’apaga un sistema Linux directament
   (cal usar `poweroff`, `shutdown` o `reboot`).

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

**Final configuració**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Seguim amb les instruccions del fitxer sobre [Personalització](03-Personalitzacio.md):
caldrà iniciar sessió gràfica amb el teu usuari i obrir el _Firefox_ per poder
consultar el fitxer [Personalització](03-Personalitzacio.md).

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
