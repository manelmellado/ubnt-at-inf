
# Instal·lació d'Ubuntu 22.04

**Curs 2021-2022**

* Alerta! Si hi ha una situació diferent a la descrita: aviseu a algun professor.



## Arrencada

Caldrà crear un usb-live de [ubuntu](https://ubuntu.com/download/desktop/thank-you?version=22.04.1&architecture=amd64.)
Per fer-ho necessitem un pen-drive d'almenys 8 Gb i un programa com [balena etcher](https://www.balena.io/etcher/) per crear el live.




+ Els ordinadors fan sel·lecció de boot. En la arrencada cal prémer F12.

+ Apareix un menú. Escollim el dispositiu USB introduït, per exemple Verbatin o Sandisk
+ Seguidament Try or install ubuntu

## Instal·lació i configuració

Un cop carregada la imatge apareix la pantalla per triar l’idioma i el que volem fer.

**Important**: triem com a idioma d’instal·lació l’**English** i prenem Install Ubuntu.

Ara cal configurar les diferents seccions que ens demanen amb el teclat o ratolí


1. _Keyboard layout_:

    Marquem _spanish_, _Spanish-Catalan_.
    

2. _Updates and other software_:

    Marquem _Normal installation_. Per instal·lar tots els paquets predeterminats
    Sel·leccionem també _ Download updates while installing ubuntu_ i _Install third-party software_.

3. _Installation type_

    Sel·leccionem _Something else_.

4. _Installation type Partition disks_
+ TARDA

        > __Alerta !!! Alerta !!! Alerta !!! Com que se suposa que ja s'han fet les particions, *només utilitzem la nostra partició*__

        Escollim opció d'instal·lació _Something else_

        - Partició del sistema Ubuntu (Tarda). Seleccionem la línia de __#6 Logical ...__ ↩,
            - Partició del sistema Debian (Tarda). Seleccionem la línia de _FREE
              BOTÓ Change,  modifiquem la partició:
            - Mida: _200000 MB_
            - Use as: _Ext4 journaling file system_
            - Mountpoint: _/_
              Cliquem OK


        - EEstablim de manera **permanent** aquests canvis: _Install Now_

         - Repassem la info i en principi si tot està bé fem definitius els canvis:
         Responem `<Continue>` a la pregunta _Write Changes to disk_

    + MATÍ

    > __Alerta !!! Alerta !!! Alerta !!! Això només es fa el primer dia d'instal·lació, altrament eliminarem el sistema operatiu del company de la tarda!!!__ 

    - Escollim opció d'instal·lació _Something else_

    - Eliminarem totes les particions del nostre dispositiu, creant una taula
      de particions nova. 
      Sel·leccionem /dev/sda o /dev/nvme0n1 segons el tipus de disc i cliquem sobre _New Partition Table_.
      Continuem per netejar el disc.

      - Partició del sistema Boot EFI. Seleccionem la línia de _FREE
        BOTÓ +,  creem una nova partició:
        - Mida: _500 MB_
        - Tipus: _Primary_
        - Lloc: _Beginning_
        - Use as: _EFI System Partition_
          Cliquem OK      

      - Partició del sistema Ubuntu (Mati). Seleccionem la línia de _FREE
        BOTÓ +,  creem una nova partició:
        - Mida: _200000 MB_
        - Tipus: _Primary_
        - Lloc: _Beginning_
        - Use as: _Ext4 journaling file system_
        - Mountpoint: _/_
          Cliquem OK
      
      - Partició del sistema Debian (Tarda). Seleccionem la línia de _FREE
        BOTÓ +,  creem una nova partició:
        - Mida: _200000 MB_
        - Tipus: _Primary_
        - Lloc: _Beginning_
        - Use as: _Ext4 journaling file system_
        - Mountpoint: _Ho deixem en blank_
          Cliquem OK

      - Partició d'intercanvi _SWAP_ (comuna). Seleccionem la línia de _FREE
        BOTÓ +,  creem una nova partició:
        - Mida: _500000 MB_
        - Tipus: _Primary_
        - Lloc: _Beginning_
        - Use as: _swap area_
          Cliquem OK

      - Establim de manera **permanent** aquests canvis: _Install Now_

        Tindrem un avís (Warning) de que hi ha una partició (la de la tarda) a
        la qual no li hem assignat cap punt de muntatge. Seleccionem `<Continue>`

      - Repassem la info i en principi si tot està bé fem definitius els canvis:
        
        Responem `<Continue>`.

5. _Where are you?_

    Establirem l'hora del sistema:

    Detectarà _Madrid_, continuem

6. _Who are you?_

    Ens demanarà l'usuari principal del sistema.
    Crearem l'usuari guest.
    
    - Your name: _guest_
    - Your computer's name: _localhost_
    - Pick a username: _guest_
    - Choose a password: _guest_
    - Confirm your password: _guest_

    Deixem marcat Require my password to log in

 
11. _Finnish the installation_

    Seleccionem `<Continue>`

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

**Final instal·lació bàsica**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

Seguim amb les instruccions del fitxer sobre [Configuració](02-Configuracio.md):
caldrà iniciar sessió gràfica com l’usuari _guest_ i obrir el _Firefox_ per poder
consultar el fitxer [Configuració](02-Configuracio.md).

<!-- 
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->

