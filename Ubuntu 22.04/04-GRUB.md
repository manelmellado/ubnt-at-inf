# Revisió final del GRUB

**Per ara no he aconseguit reinstal·lar el grub al matí, per tant manarà l'última partició instal·lada**
**Aquestes operacions les han de realitzar els alumnes del matí, el 2on dia d'instal·lació.**

Ara configurarem el GRUB correctament per a l’arrancada dual.

1. Verificació de que el sistema s’ha iniciat amb la partició del matí (`/dev/sda2` o `/dev/nvme0n1p2` ).
`
    Executem:
    - Aules amb SATA
        
		```
    	mount | grep '/dev/sda2 on /'
    	```
        
    - Aules NVE M.2:
        
    	```
    	mount | grep '/dev/nvme0n1p2' 
    	```

    Si no hi ha sortida tornem a iniciar el sistema i ara triem correctament la partició al GRUB.

    Assegurar-se de que aquesta vegada s'ha escollit l'entrada correcta tornant a executar l'ordre anterior.
    (Aquest seria un primer exemple de bucle)

    (Més endavant, amb expressions regulars podrem fer una comprovació comuna com per exemple `mount | grep  '/dev/[^ ]\+5 .* / '`) 
 
2. Instal·lem el [gestor de GRUB](https://ubuntuhandbook.org/index.php/2022/04/install-grub-customizer-ubuntu-2204/):

    - Afegim el repositori extra:
    ```
    # add-apt-repository ppa:danielrichter2007/grub-customizer
    ```
    - Instal·lem el customitzador:
    ```
    # apt install grub-customizer -y
    ```
    - Engeguem cercant Grub al menú d'aplicacions.
    - Al menú de _List configuration_ podem modificar els noms dels menús i l'ordre.
        Modificarem el primer de tots anomenat Ubuntu per `MATÍ`
        Modificarem el que faci referència al generic `/dev/sda3` o `/dev/nvme0n1p3` per `TARDA`

    
## Cal revisar aquesta part si no manarà la tarda, per ara he fet proves i no va.
    Recuperem per al grub del matí la informació de la instal·lació de la tarda:
    
    ```
    update-grub
    ```
    
    Ara tornarem a instal·lar el grub a la partició del matí:

    Aula N2H, N2J

	```
	grub-install /dev/sda
	```
    Aula N2I

	```
	grub-install /dev/nvme0n1
	```

3. Engeguem el grup customizer:

    - Cerquem a les aplicacions el Grub:
    - La primera opció anomenada Ubuntu, abans del primer bloc d'_Advanced options for Ubuntu_ Amb l'icona d'una carpeta hem de modificar el primer:
    ```
	Ubuntu, with Linux.... Per MATÍ
    ```
    - Del primer bloc d'_Advanced options for Ubuntu_, l'última opció del bloc, que farà referència a la partició de la tarda, Ubuntu 22.04...(on /dev/sda3) 
    ```
	Modifiquem aquesta entrada per TARDA
    ```
    - Cliquem en save, dalt a l'esquerra i reiniciem per comprobar.

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
