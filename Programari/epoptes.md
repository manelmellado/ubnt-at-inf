# Instal·lació epoptes versió profe

# Curs 2022-2023

## IMPORTANT 

És imprescindible que l'ordinador de professorat de l'aula tingui instal·lat el servidor d'epoptes abans que els alumnes instal·len, degut que hauran de fer una petició de confirmació a la mateixa.
Podeu trobar la guia per l'instal·lació dels alumnes al fitxer de [Personalització](../Debian%2011/03-Personalitzacio.md), punt 5.


## Instal·lació

* Com a root o sudo executem la comanda d'instal·lació.
    ```
    apt-get install --install-recommends epoptes
    ```
    Un cop instal·lat podem procedir a l'instal·lació dels clients a les màquines de l'alumnat.

    Per poder executar el programa cal incloure cada usuari al grup epoptes. $USER fa referència a l'usuari de cadascú.

    ```
    gpasswd -a $USER epoptes
    ```

* Amb un reinici o log out, ja serem capaços d'obrir el programari epoptes i veure totes les màquines connectades a la xarxa.

