# Rebuda del portàtil docent

# Primeres passes

## IMPORTANT 

Per tal de realitzar la perimera connexió és **imprescindible** tenir el portàtil connectat a internet.

Algunes versions no permeten la connexió via wifi fins que no has fet el primer log in, per tant, en aquests casos caldrà connectar-se per cable.
Trobareu la toma RJ-45 per al cable de xarxa a una pestanyeta a l'esquerra del portàtil. Te una mica de mobilitat, com una palanqueta, per que pugui entrar el connector.


## Inici de la sessió

* Per iniciar sessió utilitzarem el nostre usuari de l'xtec, per exemple en el meu cas mmella3, seguit del domini @edu.gencat.cat. Quedant de la següent manera:

    ```
    mmella3@edu.gencat.cat
    ```
 * La contrasenya en canvi, farem servir la de L'ATRI / GICAR, la mateixa que utilitzem al saga. Teniu en compte que aquesta contrasenya caduca i cal renovar-la de tant en tant.
 * En cas de no recordar la contrasenya del atri, us aconselle cercar-la o renovar-la, degut que amb tan sols 2 intents fallits us bloquejarà el compte i necessitareu [recuperar-la](https://autenticaciogicar4.extranet.gencat.cat/autogestio/iniCaptcha.jsp) igualment.
 
 *  
    Un cop instal·lat podem procedir a l'instal·lació dels clients a les màquines de l'alumnat.
