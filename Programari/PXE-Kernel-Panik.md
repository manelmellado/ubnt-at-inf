# Resolució error Kernel Panic PXE

# Curs 2022-2023

## Explicació de l'error

* En entrar al PXE i sel·leccionar el operatiu, en aquest exemple debian 11, pot ocorrer que hi haja un mismatch de versions, entre la versió de la iso-netinst i el kernel al que fa referència. Açò és degut que la iso cerca a internet l'última versió però el kernel està descarregat al sistema.

## Solució de l'error

* Per solucionar l'error cal tornar a descarregar a la carpeta corresponent el nou kernel. Utilitzarem com a exemple el PXE que localitzem a gandhi.
* Els passos a seguir son els següents:
  
1. Accedim al directori "/var/lib/tftpboot/fedora-install/" on trobarem totes les entrades de PXE disponibles al sistema.
    ```
    cd /var/lib/tftpboot/fedora-install/
    ```
2. Trobarem el fitxer de configuració del menú, on podem comprovar on estan els fitxers que fan referència al sistema que desitgem a:
   ``` 
   /var/lib/tftpboot/fedora-install/pxelinux.cfg/default
   ``` 
En aquest cas ens centrarem en l'entrada de debian 11 bullseye
   ``` 
    # # Instal·lació Debian BullsEye 3
   LABEL Debian11
     MENU LABEL Debian 11.0 (Bullseye)
     KERNEL debian-install/debian11_3/linux
     APPEND initrd=debian-install/debian11_3/initrd.gz inst.repo=ftp://gandhi/pub/debian-11.0.0-amd64-netinst/
     TEXT HELP
     Instal.lador "Netinstall" des de GANDHI amb paquets en repositori remot
     ENDTEXT

   MENU SEPARATOR
   ``` 
   En aquest cas, els fitxers que ens interesa modificar seran els que es troben al directori debian11_3, tant linux com initrd.gz.

3. Un cop aquí, localitzem el directori on es troba l'afectació, en este cas "debian-install". 
    ```
    cd debian-install/debian11_3/
    ``` 
    Per defecte trobarem solament el directori amd64 a l'interior, en aquest cas degut les modificacions hi ha extra.
    ``` 
    ls    
    amd64  debian11  debian11_2  debian11_3
    ``` 

4. Per seguretat podem mantenir els fitxers antics, be movent-los dintre del propi debian11_3 a un directori OLD o fent un nou debian11_4. En cas de la segona opció, caldrà modificar la ruta del fitxer que parlem al punt 2.
5. Cal ara descarregar els nous fitxers del kernel. Els trobarem al repositori oficial de la distribució, en aquest cas:

    [http://debian.mirror.constant.com/dists/bullseye/main/installer-amd64/current/images/netboot/debian-installer/amd64/](http://debian.mirror.constant.com/dists/bullseye/main/installer-amd64/current/images/netboot/debian-installer/amd64/)
    
    Un cop localitzats els fitxers a descarregar, ens situem al directori corresponent, en aquest cas debian11_3. I a continuació els descarreguem.
    ```
    cd /var/lib/tftpboot/fedora-install/debian-install/debian11_3/
    
    wget http://debian.mirror.constant.com/dists/bullseye/main/installer-amd64/current/images/netboot/debian-installer/amd64/initrd.gz
    
    wget http://debian.mirror.constant.com/dists/bullseye/main/installer-amd64/current/images/netboot/debian-installer/amd64/linux
    ```

6. Iniciem qualsevol màquina per PXE i comprovem que la descàrrega ha sigut correcta i el kernel panic ja no apareix.

